﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Video_Streaming_0_1.Models;

namespace Video_Streaming_0_1.Controllers
{
    /// <summary>
    /// this code is a sample of simple video streaming.
    /// when you want show a video and limited access to it you can use this 
    /// More over You can use session or other identity user for limited and ban automatic download like IDM
    /// in the end this is a simpale way please send your suggest to us.
    /// just for front end please ban  right click
    /// for more support :mhhr1990@gmail.com
    /// mehdi.khosrojerdy@gmail.com  
    /// </summary>
    public class VideoPlayerController : Controller
    {
        public class indexViewModel
        {
            public string FilePaly { set; get; }
            public string Poster { set; get; }
        }
        Video_Streaming_0_1.Models.VideoStreamDBEntities db = new Models.VideoStreamDBEntities();
        // GET: VideoPlayer
        public ActionResult Index()
        {
            indexViewModel result = new indexViewModel();
            LogDownlodTBL logDownlodTBL = new LogDownlodTBL();// we create a new roq 
            logDownlodTBL.IDCustomerTBL = 1;//"you shoul insert identity param for your user we use it for limited user and Download Atoumatic such as  IDM"
            logDownlodTBL.IDFileTBL =1;//if you have table for file please insert identity param in this varilable
            string templog = Guid.NewGuid().ToString();
            logDownlodTBL.Link = templog;
            logDownlodTBL.DateDownload = DateTime.Now;
            logDownlodTBL.Status = 1;// when you get file this change to zero
            db.LogDownlodTBLs.Add(logDownlodTBL);
            db.SaveChanges();
            result.FilePaly = templog;
            return View(result);
        }
    }
}