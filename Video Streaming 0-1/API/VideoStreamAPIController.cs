﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Hosting;
using System.IO;
using System.Net.Http.Headers;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace Video_Streaming_0_1.API
{
    public class VideoStreamAPIController : ApiController
    {
        Video_Streaming_0_1.Models.VideoStreamDBEntities db = new Models.VideoStreamDBEntities();
        [HttpGet]
        public HttpResponseMessage FromVideo(string videoName)
        {
            //actually videoname is key and refer to one row of logDowloadTBL in db
            //in this case we user guid
            if (videoName != null)
            {
                var item = db.LogDownlodTBLs.Where(o => o.Link == videoName).OrderByDescending(o => o.ID).First();
                if (item.Status == 1)
                {
                    /// in this palce you can set any limited such as login verify or limite count download per user
                    item.Status = 0;
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                   /// videoName =your File name
                   /// this fill vieoname var you have IDFileTBL so  get that and pass to VideoName
                
                
                    var video = new VideoStream(videoName);//create your video function and streaming
                    Func<Stream, HttpContent, TransportContext, Task> func = video.WriteToStream;
                    var response = Request.CreateResponse();
                    response.Content = new PushStreamContent(func, new MediaTypeHeaderValue("video/mp4"));
                    return response;
                }
            }
            return null;

        }
        internal class VideoStream
        {
            private readonly string videoName;

            public VideoStream(string videoName)
            {
                this.videoName = videoName;
            }

            public async Task WriteToStream(Stream outputStream, HttpContent content, TransportContext context)
            {
                // string videoFileName = + videoName;
                string videoFileName = @"Your file address please check below address and you shoul write a address in this format not with your address " + videoName;
                //address sample 
                //@"C:\HostingSpaces\wwwroot\Areas\Administrator\File\Product\podcast\"
                // this address is phisical address in your server 
                try
                {
                    var buffer = new byte[65536];

                    using (var video = File.Open(videoFileName, FileMode.Open, FileAccess.Read))// we use from this function for open file and create stream
                    {
                        var length = (int)video.Length;//file length use for Conditional while
                        var bytesRead = 1;

                        while (length > 0 && bytesRead > 0)// read until finish file
                        {
                            bytesRead = video.Read(buffer, 0, Math.Min(length, buffer.Length));
                            await outputStream.WriteAsync(buffer, 0, bytesRead);
                            length -= bytesRead;
                        }
                    }
                }
                catch (Exception ex)
                {
                    return;
                }
                finally//when finish reading file is finish
                {
                    outputStream.Close();
                }
            }
        }
    }
}
